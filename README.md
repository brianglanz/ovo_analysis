# OpenVirtualObjects (OVO) 

OpenVirtualObjects (OVO) are 3D household objects for virtual reality-based research, assessment, and therapy -- more than 100 of them, all standardized, validated, and free and open. 

## TLDR

* 🧾Preprint: [https://osf.io/ejkzt/](https://osf.io/ejkzt/)
* 📈Data & analysis scripts: right here! and originally at https://gitlab.gwdg.de/johanne.tromp01/ovo_analysis
* 🧰Objects, Unity code, and the experiment: https://edmond.mpdl.mpg.de/imeji/collection/7L7t07UXD8asG_MI 


![Example objects](/figures/Figure1.png)

The objects were rated for Recognizability, Familiarity, Details (i.e., visual complexity), Contact, Usage (i.e., frequency of usage in daily life) by older and younger adults. The participants also named and categorised the objects.

This repository contains the analyses described in: OpenVirtualObjects (OVO): An open set of standardized and validated 3D household objects for virtual reality-based research, assessment, and therapy (add link to paper)

* **Descriptive statistics** of all dimensions for older and younger adults.
* **Correlation analysis** for dimensions.
* **Name Agreement (NA)** and **H-statistic** for object names for older and younger adults.
* Distribution of objects over different **categories** for older and younger adults.
* Comparison with existing object/photo **databases**.

## Getting started

### Prerequisites

How to clone a GitLab repository: https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#clone-a-repository

How to install R and Rstudio: https://rstudio.com/products/rstudio/download/


### Analysis

* Please run all analyses (OVO_analysis_complete.Rmd) from within the .Rproj 'ovo_analysis'

* For a quick overview of the dimensions per object consider:
obj_output_all.csv 


## Contributing

## Authors

* Johanne Tromp, Felix Klotzsche, Michael Gaebler


